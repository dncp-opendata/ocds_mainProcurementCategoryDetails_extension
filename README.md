# ocds_mainProcurementCategoryDetails_extension

Additional detail on the main procurement category used. This field can be used to provide the local name of the particular procurement category used.
